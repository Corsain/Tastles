﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {



	public enum GameMode{SinglePlayer, Multiplayer}
	public GameMode gameMode;

	public int AINum = 0;

	public GameObject SinglePlayer;
	public GameObject MultiPlayer1;
	//public GameObject MultiPlayer2;

	public GameObject playerTank;
	public GameObject playerCopy;
	//public GameObject player2Tank;
	//public GameObject playerCopy;

	public GameObject GhostTank;

	public GameObject Spawner1;
//	public GameObject Spawner2;

	public GameObject[] AISpawns;

	public Camera OptionsMenuCam;
	public Camera MainMenuCam;
	public Camera GameCam;
	public Camera GamCam1;
	public Camera GamCam2;

//	public GameObject VictoryCam;
//	public GameObject GameOverCam;

	public Camera VictoryCamera;
	public Camera GameOverCamera;

	[HideInInspector] public bool hasGameStarted;

	public bool isGameOver;

	public static GameManager instance;


	void Awake()
	{
		hasGameStarted = false;

		if (instance == null) 
		{
			instance = this;
		}
		
		else
		{
			Destroy(gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		isGameOver = false;

		MainMenuCam.enabled = true;
		OptionsMenuCam.enabled = false;
		VictoryCamera.enabled = false;
		GameOverCamera.enabled = false;



	}
	
	// Update is called once per frame
	void Update () 
	{
		if (playerCopy == null && hasGameStarted)
		{
			GameOver();
		}
	}


	public void MainChange()
	{
		OptionsMenuCam.enabled = false;
		MainMenuCam.enabled = true;
	}


	public void OptionsChange()
	{
		OptionsMenuCam.enabled = true;
		MainMenuCam.enabled = false;
	}

	public void SpawnPlayer()
	{
		switch(gameMode)
		{
		case GameMode.SinglePlayer:
			playerTank = SinglePlayer;
			playerCopy = Instantiate(playerTank,Spawner1.transform.position
			                              ,Quaternion.identity)as GameObject;
			break;
		case GameMode.Multiplayer:
			playerTank = MultiPlayer1;
			playerCopy = Instantiate(playerTank,Spawner1.transform.position
			                               ,Quaternion.identity)as GameObject;
			//GameObject temp2 = Instantiate(MultiPlayer2,Spawner2.transform.position
			                           //    ,Quaternion.identity)as GameObject;
			break;

		}
	}

	public void ChangeGameMode(GameMode newMode)
	{
		gameMode = newMode;
	}

	public void NowSingle()
	{
		ChangeGameMode (GameMode.SinglePlayer);
	}

	public void NowMulti()
	{
		ChangeGameMode (GameMode.Multiplayer);
	}

	public void NowPlayTime()
	{
		SpawnPlayer ();
		switch(gameMode)
		{
		case GameMode.SinglePlayer:
			GameCam.enabled = true;
			MainMenuCam.enabled = false;
			break;
		case GameMode.Multiplayer:
			GamCam1.enabled = true;
			GamCam2.enabled = true;
			MainMenuCam.enabled = false;
			break;
		}
		SpawnAI ();
		isGameOver = false;
		hasGameStarted = true;
	}

	public void SpawnAI()
	{
		for (int i = 0; i < AISpawns.Length; i++)
		{
			GameObject temp = Instantiate(GhostTank, AISpawns[i].transform.position, Quaternion.identity) as GameObject;
		}
	}

	public void KillAI()
	{
		AINum -= 1;

		if (AINum < 1)
		{
			GameOver();
		}


	}

	public void GameOver()
	{
		isGameOver = true;

		if (AINum > 0)
		{
			ShowLose();
		}

		else
		{
			ShowWin();
		}
	}

	void ShowLose()
	{
		switch(gameMode)
		{
		case GameMode.SinglePlayer:
			GameCam.enabled = false;
			//GameOverCam.SetActive(true);
			GameOverCamera.enabled = true;
			break;
		case GameMode.Multiplayer:
			GamCam1.enabled = false;
			GamCam2.enabled = false;
			//GameOverCam.SetActive(true);
			GameOverCamera.enabled = true;
			break;
		}
	}

	void ShowWin()
	{
		//Debug.Log ("boop");
		switch(gameMode)
		{
		case GameMode.SinglePlayer:
			GameCam.enabled = false;
			//VictoryCam.SetActive(true);
			VictoryCamera.enabled = true;
			break;
		case GameMode.Multiplayer:
			GamCam1.enabled = false;
			GamCam2.enabled = false;
			//VictoryCam.SetActive(true);
			VictoryCamera.enabled = true;
			break;
		}
	}
}
