﻿using UnityEngine;
using System.Collections;

public class PlayerData : MonoBehaviour {

	[Range(180,720)]
	public float rotateSpeed = 180f;

	[Range(1,50)]
	public float moveSpeed = 3f;

	public int score = 0;

	public float rotateCamSpeed = 90f;

	public int lives = 0;

	public float backSpeed = 1;



}
