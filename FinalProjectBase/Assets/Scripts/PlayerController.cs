﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public enum PlayerType{player1, player2}//enum for player type
	public PlayerType playertype;

	public enum PlayerStation{normal, cannon, driver} // enum for the stations
	public PlayerStation station;

	public GameObject Tastle;
	public GameObject Cannon;
	public GameObject DriverDetecter;
	public GameObject CannonDetect;

	public CastleMotor cmotor;
	public CastleData cdata;

	public CannonMotor camotor;
	public CannonShooter cashoot;

	public PlayerData data;
	public PlayerMotor motor;
	public PlayerDetecter detect;
	public CannonDetecter cdetect;

	
	// Use this for initialization
	void Start () {
		data = gameObject.GetComponent<PlayerData> ();
		motor = gameObject.GetComponent<PlayerMotor> ();
		cmotor = Tastle.GetComponent<CastleMotor> ();
		cdata = Tastle.GetComponent<CastleData> ();
		camotor = Cannon.GetComponent<CannonMotor>();
		cashoot = Cannon.GetComponent<CannonShooter>();
		detect = DriverDetecter.GetComponent<PlayerDetecter>();
		cdetect = CannonDetect.GetComponent<CannonDetecter>();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		switch (station){
		case PlayerStation.normal:// player controls in the normal state
			switch (playertype) {
			case PlayerType.player1:// player 1 controls
				if (Input.GetKey (KeyCode.W)) {
					motor.Move (data.moveSpeed);
				}
				if (Input.GetKey (KeyCode.S)) {
					motor.Move (-data.moveSpeed);
				}
				if (Input.GetKey (KeyCode.A)) {
					motor.Rotate (-data.rotateSpeed);
				}
				if (Input.GetKey (KeyCode.D)) {
					motor.Rotate (data.rotateSpeed);
				}
				if (Input.GetKey (KeyCode.E)) {
					if(detect.playerDetect == true)
					{
						PlayerStateSwitch(PlayerStation.driver);
					}
					if(cdetect.cannonstation == true)
					{
						PlayerStateSwitch(PlayerStation.cannon);
					}
				}

				break;
			
			case PlayerType.player2:// player 2 controls
				if (Input.GetAxis("Vertical") > 0) {
					motor.Move (data.moveSpeed);
				}
				if (Input.GetAxis("Vertical") < 0) {
					motor.Move (-data.moveSpeed);
				}
				if (Input.GetAxis("Horizontal") > 0) {
					motor.Rotate (data.rotateSpeed);
				}
				if (Input.GetAxis("Horizontal") < 0) {
					motor.Rotate (-data.rotateSpeed);
				}
				if (Input.GetKey (KeyCode.Joystick1Button0)) {
					if(detect.playerDetect == true)
					{
						PlayerStateSwitch(PlayerStation.driver);
					}else

					if(cdetect.cannonstation == true)
					{
						PlayerStateSwitch(PlayerStation.cannon);
					}

				}

				break;
			default:
				break;
			}
			break;
		case PlayerStation.driver:// players controls in the driving state
			switch (playertype) {
			case PlayerType.player1:// player 1 controls
				if (Input.GetKey (KeyCode.W)) {
					cmotor.CastleMove(cdata.castleMoveSpeed);
				}
				if (Input.GetKey (KeyCode.S)) {
					cmotor.CastleMove(-cdata.castleMoveSpeed);
				}
				if (Input.GetKey (KeyCode.A)) {
					cmotor.CastleRotate(-cdata.castleRotateSpeed);
				}
				if (Input.GetKey (KeyCode.D)) {
					cmotor.CastleRotate(cdata.castleRotateSpeed);
				}

				if (Input.GetKey (KeyCode.Q)) {
					PlayerStateSwitch(PlayerStation.normal);
				}
				break;
				
			case PlayerType.player2:// player 2 controls
				if (Input.GetAxis ("Vertical") < 0) {
					cmotor.CastleMove(-cdata.castleMoveSpeed);
				}
				if (Input.GetAxis("Vertical") > 0) {
					cmotor.CastleMove(cdata.castleMoveSpeed);
				}
				if (Input.GetAxis("Horizontal") > 0) {
					cmotor.CastleRotate(-cdata.castleRotateSpeed);
				}
				if (Input.GetAxis("Horizontal") > 0) {
					cmotor.CastleRotate(cdata.castleRotateSpeed);
				}

				if (Input.GetKey (KeyCode.Joystick1Button1)) {
					PlayerStateSwitch(PlayerStation.normal);
				}
				break;
			default:
				break;
			}
			break;

		case PlayerStation.cannon: // The players controls while in the cannon state
			switch (playertype) {
			case PlayerType.player1:// player 1 controls
				if (Input.GetKey (KeyCode.W)) {
					camotor.RotateX(-cdata.cannonRotateSpeed);
				}
				if (Input.GetKey (KeyCode.S)) {
					camotor.RotateX(cdata.cannonRotateSpeed);
				}
				if (Input.GetKey (KeyCode.A)) {
					camotor.RotateY(-cdata.cannonRotateSpeed);
				}
				if (Input.GetKey (KeyCode.D)) {
					camotor.RotateY(cdata.cannonRotateSpeed);
				}

				if (Input.GetKey (KeyCode.Q)) {
					PlayerStateSwitch(PlayerStation.normal);
				}

				if (Input.GetKeyDown(KeyCode.E))
				{
					cashoot.Shoot();
				}
				break;
				
			case PlayerType.player2:// player 2 controls
				if (Input.GetAxis("Vertical") < 0) {
					camotor.RotateX(-cdata.cannonRotateSpeed);
				}
				if (Input.GetAxis("Vertical") < 0) {
					camotor.RotateX(cdata.cannonRotateSpeed);
				}
				if (Input.GetAxis("Horizontal") > 0) {
					camotor.RotateY(-cdata.cannonRotateSpeed);
				}
				if (Input.GetAxis("Horizontal") > 0) {
					camotor.RotateY(cdata.cannonRotateSpeed);
				}

				if (Input.GetKey (KeyCode.Joystick1Button1)) {
					PlayerStateSwitch(PlayerStation.normal);
				}

				if (Input.GetKeyDown (KeyCode.Joystick1Button0)) 
				{
					cashoot.Shoot();
				}
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	public void PlayerStateSwitch(PlayerStation newStation)
	{
		station = newStation;
	}
}
