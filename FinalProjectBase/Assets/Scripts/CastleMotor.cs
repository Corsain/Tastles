﻿using UnityEngine;
using System.Collections;

public class CastleMotor : MonoBehaviour {

	public Transform tf;
	public CharacterController con;

	// Use this for initialization
	void Start () 
	{
		tf = gameObject.GetComponent<Transform> ();
		con = gameObject.GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CastleMove(float speed)
	{
		con.SimpleMove(tf.forward * speed);
	}

	public void CastleRotate(float speed)
	{
		tf.Rotate (new Vector3 (0, speed * Time.deltaTime, 0));
	}

	public bool RotateTowards ( Vector3 target, float speed )
	{
		// Find the quaternion towards our target
		Quaternion targetRotation = Quaternion.LookRotation (target - tf.position );
		
		// Rotate slightly towards that rotation
		tf.rotation = Quaternion.RotateTowards (tf.rotation, targetRotation, speed * Time.deltaTime);
		
		// If we are looking at our target, return true
		if (tf.rotation == targetRotation) {
			return true;
		} else {
			// Else return false
			return false;
		}
	}
}
