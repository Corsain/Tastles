﻿using UnityEngine;
using System.Collections;

public class PlayerMouseMovement : MonoBehaviour {

	public PlayerData data;
	public enum PlayerCam{PlayerCam1, PlayerCam2}
	public PlayerCam playerCam;

	// Use this for initialization
	void Start () {
		data = gameObject.GetComponent<PlayerData> ();
	}
	
	// Update is called once per frame
	void Update () {
		switch(playerCam)
		{
		case PlayerCam.PlayerCam1:
		if(Input.GetAxis("Mouse X")<0)
		{
			//transform.Rotate(new Vector3(0,-data.rotateCamSpeed * Time.deltaTime,0));
			transform.Rotate(transform.up * -data.rotateSpeed * Time.deltaTime);

			Fix ();
		}

		if(Input.GetAxis("Mouse X")>0)
		{
			//transform.Rotate(new Vector3(0,data.rotateCamSpeed * Time.deltaTime,0));
			transform.Rotate(transform.up * data.rotateSpeed * Time.deltaTime);

			Fix ();
		}

		if(Input.GetAxis("Mouse Y")>0)
		{
			//transform.Rotate(new Vector3(-data.rotateSpeed * Time.deltaTime,0,0), Space.World);
			//transform.rotation.x += data.rotateSpeed * Time.deltaTime;
			//transform.Rotate(transform.right * -data.rotateSpeed * Time.deltaTime);
			transform.RotateAround(transform.position, transform.right, -data.rotateSpeed * Time.deltaTime);

			Fix ();

		}
		
		if(Input.GetAxis("Mouse Y")<0)
		{
			//transform.Rotate(new Vector3(data.rotateSpeed * Time.deltaTime,0,0), Space.World);
			//transform.Rotate(
			//transform.Rotate(transform.right * data.rotateSpeed * Time.deltaTime);
			transform.RotateAround(transform.position, transform.right, data.rotateSpeed * Time.deltaTime);

			Fix ();

		}
			break;
		case PlayerCam.PlayerCam2:
			if (Input.GetAxis("RightJoyStickHori") < 0) 
		{
			transform.Rotate(transform.up * data.rotateSpeed * Time.deltaTime);
			
			Fix ();
		}

			if (Input.GetAxis("RightJoyStickHori") > 0) 
		{
			transform.Rotate(transform.up * -data.rotateSpeed * Time.deltaTime);
			
			Fix ();
		}

			if(Input.GetAxis("RightJoyStickVert") > 0)
		{
			//transform.Rotate(new Vector3(data.rotateSpeed * Time.deltaTime,0,0), Space.World);
			//transform.Rotate(
			//transform.Rotate(transform.right * data.rotateSpeed * Time.deltaTime);
			transform.RotateAround(transform.position, transform.right, -data.rotateSpeed * Time.deltaTime);
			
			Fix ();
			
		}

			if(Input.GetAxis("RightJoyStickVert") < 0)
		{
			//transform.Rotate(new Vector3(data.rotateSpeed * Time.deltaTime,0,0), Space.World);
			//transform.Rotate(
			//transform.Rotate(transform.right * data.rotateSpeed * Time.deltaTime);
			transform.RotateAround(transform.position, transform.right, data.rotateSpeed * Time.deltaTime);
			
			Fix ();
			
		}
			break;
		}
	}



	void Fix()
	{
		Quaternion tempRot = new Quaternion ();
		tempRot.eulerAngles = new Vector3 (transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0);
		transform.rotation = tempRot;
	}
}
