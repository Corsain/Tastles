﻿using UnityEngine;
using System.Collections;

public class BulletScript : MonoBehaviour {

	//Bullet stat variables
	public float bulletSpeed = 1000.0f; //how fast the bullet moves
	public float maxDistance = 16f;
	public int damage = 1; //how many lives the bullet takes off when hitting a tank

	//Variables to determine when bullet will be destroyed
	public float lifeSpan = 3.0f;
	public float timeWhenDie;


	//variable for rigidbody
	public Rigidbody rb;

	// Use this for initialization
	void Start () 
	{
		rb = gameObject.GetComponent<Rigidbody> ();

		rb.AddForce (transform.forward * bulletSpeed);
		//Set when bullet will despawn by using lifespan and the current time
		timeWhenDie = Time.time + lifeSpan;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//If enough time has passed since the bullet was shot...
		if (timeWhenDie < Time.time) 
		{
			Destroy(gameObject); //destroy the bullet
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		Destroy (gameObject);
	}
}
