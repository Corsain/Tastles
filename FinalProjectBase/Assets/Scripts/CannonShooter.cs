﻿using UnityEngine;
using System.Collections;

public class CannonShooter : MonoBehaviour 
{
	public Transform barrel;

	public GameObject bullet;

	//public float bulletSpeed = 400.0f;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Shoot()
	{
		Rigidbody projectile = Instantiate(bullet, barrel.position, barrel.rotation) as Rigidbody; //create a bullet and launch it from a specified location.
		
		//projectile.AddForce (transform.forward * bulletSpeed); //Give bullet its forward momentum
	}
}
