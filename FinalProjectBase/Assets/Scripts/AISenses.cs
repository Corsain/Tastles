﻿using UnityEngine;
using System.Collections;

public class AISenses : MonoBehaviour {
	//Put variables for the AI's sight stats
	public float viewAngle = 50f;
	public float viewLength = 10f;
	
	//How far the AI can hear player shooting
	public float hearLength = 10f;
	
	//bool for it player is within the sphere collider
	public bool playerInRange;
	
	//bools for if tank can see and hear the player
	public bool playerSeen;
	public bool playerHeard;

	//variables for components
	private Transform tf;
	public Transform target;

	// Use this for initialization
	void Start () 
	{
		tf = gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Check if we can hear player
		playerHeard = PlayerHeard ();
		
		//Check if we can see player
		playerSeen = PlayerIsSeen ();
	}

	//Function for if player is within the sight angle and sight distance
	bool PlayerInRange()
	{
		float distance = Vector3.Distance (target.position, tf.position);
		
		//If player/target is withing the view length...
		if (distance <= viewLength) 
		{
			
			//Vector of tank to the player
			Vector3 playerDirection = target.position - tf.position;
			
			//Vector of tank to max sight distance
			Vector3 sight = tf.forward;
			
			//Make the angle between those two
			float currAngle = Vector3.Angle (playerDirection, sight);
			
			//if that angle is within the tank's sight angle...
			if (currAngle <= viewAngle) 
			{
				return true; //return true
			} 
			
			//if not...
			else 
			{
				return false; //return false
			}
		} 
		
		//If not close enough...
		else 
		{
			return false; //return false
		}
		
	}

	bool PlayerHeard()
	{
		if (playerInRange) 
		{
			//Make a distance between player and AI
			float distance = Vector3.Distance (target.position, tf.position);
			
			//if that distance is within how far the AI can hear...
			if (distance <= hearLength)
			{
				return true; //Return true
			}
			
			//otherwise...
			else
			{
				return false; //return false
			}
		}
		
		//If player isn't close enough for anything...
		else
		{
			return false; //return false
		}
	}

	//function to determine if player is behind cover
	bool PlayerNotBehindStuff()
	{
		//Make a distance based off of the player and the AI
		float distance = Vector3.Distance (target.position, tf.position);
		//Make a direction based off of the same thing
		Vector3 direction = target.position - tf.position;
		
		//Make a list of hits from the AI to the player
		RaycastHit[] hits = Physics.RaycastAll (tf.position, direction, distance);
		
		//For the hits in that list...
		foreach (RaycastHit hit in hits) 
		{
			
			//If any of them were a Cube/wall before the player...
			if (hit.transform.tag == "Cube")
			{
				return false; //Player is behind stuff so return false
			}
			
			//If an enemy is in the way...
			if (hit.transform.tag == "Enemy")
			{
				continue; //Keep going because enemies don't count
			}
			
			//If player is in the way before any walls...
			if (hit.transform.tag == "Player")
			{
				return true; //Player isn't behind stuff so return true
			}
		}
		
		return true; //Return true by default
	}

	//Function to determine if tank is able to totally see player
	bool PlayerIsSeen()
	{
		if (playerInRange) //If player is in the sphere collider's range...
		{
			if (PlayerInRange ()) //If player is in the distance and angle range.....
			{
				return (PlayerNotBehindStuff()); //Return whether or not player is behind stuff
			}
			
			//otherwise...
			else
			{
				return false; //Player can't be seen
			}
		} 
		else 
		{
			return false; //Player can't be seen;
		}
	}
	
	void OnTriggerStay(Collider col)
	{
		if (col.tag == "Player") 
		{
			playerInRange = true;
			target = col.transform;
		}
	}
	
	void OnTriggerExit(Collider col)
	{
		if ( col.tag == "Player")
		{
			playerInRange = false;
		}
	}
}
