﻿using UnityEngine;
using System.Collections;

public class CastleData : MonoBehaviour 
{
	public int HP;

	public float castleMoveSpeed;
	public float castleRotateSpeed;

	public float cannonRotateSpeed;

	void CheckDead()
	{
		if (HP <= 0)
		{
			Destroy(gameObject);
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject.tag == "Bullet")
		{
			HP -= 1;

			CheckDead();
		}
	}

}
