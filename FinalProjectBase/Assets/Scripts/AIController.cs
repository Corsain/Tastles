﻿using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour {
	
	public GameObject cannon;

	public CastleMotor cmotor;
	public CastleData cdata;
	
	public CannonMotor camotor;
	public CannonShooter cashoot;

	//private AISenses senses;

	public GameObject player;

	public bool shoot = false;
	public float timeBetweenShoot = 1.5f;
	[HideInInspector] public float timeUntilShoot;

	public float targetDistance;
	public float AIRange;

	private Vector3 adjustedTarget;

	// Use this for initialization
	void Start () 
	{
		GameManager.instance.AINum += 1;

		cmotor = gameObject.GetComponent<CastleMotor> ();
		cdata = gameObject.GetComponent<CastleData> ();

		camotor = cannon.GetComponent<CannonMotor> ();
		cashoot = cannon.GetComponent<CannonShooter> ();

		timeUntilShoot = timeBetweenShoot;

		SetTarget ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!GameManager.instance.isGameOver)
		{

			//Check to see if enough time has passed
			if (timeUntilShoot < Time.time) 
			{
				if (shoot)
				{
					//Tell component to shoot
					//BroadcastMessage("Shoot");
					cashoot.Shoot();
				
					//Reset the delay for the next interval
					timeUntilShoot = Time.time + timeBetweenShoot;
				}
			}

			if (player == null) 
			{
				SetTarget();
			}


			DoStuff ();
		}

		if(transform.position.y < -4)
		{
			Destroy(gameObject);
		}
		


	}

	public void SetTarget()
	{
		player = GameManager.instance.playerCopy;
	}

	void DoStuff()
	{
		targetDistance = Vector3.Distance (player.transform.position, cmotor.tf.position);

		if (targetDistance <= AIRange && AIRange - targetDistance < 2) 
		{
			if (camotor.RotateTowards(adjustedTarget, cdata.cannonRotateSpeed))
			{
				shoot = true;
			}

			else
			{
				shoot = false;
			}
		}

		else if (targetDistance <= AIRange)
		{
			adjustedTarget = new Vector3 (player.transform.position.x, cmotor.tf.position.y, player.transform.position.z);
			
			// Turn to look at the next waypoint, 
			// if looking at waypoint when finished this will return true
			if (cmotor.RotateTowards (adjustedTarget, cdata.castleRotateSpeed)) 
			{
				// Move forward
				cmotor.CastleMove(-cdata.castleMoveSpeed);
				
			}
		}

		else
		{
			shoot = false;

			adjustedTarget = new Vector3 (player.transform.position.x, cmotor.tf.position.y, player.transform.position.z);

			// Turn to look at the next waypoint, 
			// if looking at waypoint when finished this will return true
			if (cmotor.RotateTowards (adjustedTarget, cdata.castleRotateSpeed)) 
			{
				// Move forward
				cmotor.CastleMove(cdata.castleMoveSpeed);

			}
		}
	}

	void OnDestroy()
	{
		GameManager.instance.KillAI();
	}
}
