﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour 
{
	public CharacterController cc;
	private Transform tf;

	// Use this for initialization
	void Start () 
	{
		cc = gameObject.GetComponent<CharacterController> ();
		tf = gameObject.GetComponent<Transform> ();
	}
	
	public void Move(float speed)
	{
		cc.SimpleMove(tf.forward * speed);
	}

	public void Rotate(float speed)
	{
		tf.Rotate(new Vector3(0,speed * Time.deltaTime,0));
	}

}
